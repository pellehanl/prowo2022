module Peano where

data Nat = Zero | Succ Nat deriving (Show, Eq)

add :: Nat -> Nat -> Nat
add Zero b = b
add (Succ a) b = Succ $ add a b

mul :: Nat -> Nat -> Nat
mul Zero b = Zero
mul (Succ a) b = add (mul a b) b


