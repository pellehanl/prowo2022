module Two where

type Row = [Int]
type Rop = (Row -> Row)

--merge :: Rop
merge [] = []
merge (x:xs) = if uncurry (==) x && fst x /= 0 then succ (fst x) : merge (tail xs) else fst x : merge xs

