module Functions where

map1 :: (a -> b) -> [a] -> [b]
map1 _ [] = []
map1 f (x:xs) = f x : map1 f xs

map2 :: (a -> b) -> [a] -> [b]
map2 f = foldr (\x y -> f x : y) [] 

map3 :: (a -> b) -> [a] -> [b]
map3 f l = [f x | x <- l]

fold1 :: (a -> b -> a) -> a -> [b] -> a
fold1 f id [x] = f id x
fold1 f id (x:xs) = fold1 f (f id x) xs
fold1 _ _ [] = []

any1 :: (a -> Bool) -> [a] -> Bool
any1 _ [] = False
any1 f (x:xs) = f x || any1 f xs

