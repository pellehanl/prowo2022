module Rpn where

import Control.Monad
import Text.ParserCombinators.Parsec

data El = Op Char | Val Int deriving Show

int :: Parser El
int = Val . read <$> many digit

op :: Parser El
op = Op <$> oneOf "+-*/"

expr :: Parser [El]
expr = many1 ((try op <|> int) <* char ' ')

step :: [Int] -> El -> [Int]
step stack (Val v) = v:stack
step (x:xs:xxs) (Op '+') = x+xs:xxs
step (x:xs:xxs) (Op '*') = x*xs:xxs
step (x:xs:xxs) (Op '-') = x-xs:xxs
step (x:xs:xxs) (Op '/') = x `div` xs:xxs
step x _ = x

eval l = let [x] = ev l in x where
    ev:: [El] -> [Int]
    ev = foldl step []

calculate str = liftM eval (parse expr "" $ str++" ")


