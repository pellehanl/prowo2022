module Basics where

rev :: [a] -> [a]
rev [] = []
rev (x:xs) = reverse xs ++ [x]

rever :: [a] -> [a]
rever = foldl (flip (:)) []

sumrec [] = 0
sumrec (x:xs) = x + sumrec xs

sumfold :: Num a => [a] -> a
sumfold  = foldr (+) 0

appTwice :: (a -> a) -> a -> a
appTwice f = f.f

appN :: (a -> a) -> Int -> a -> a
appN f n = foldl (.) id (replicate n f)



-- Int stuff

divs :: Integral a => a -> [a]
divs n = [x | x<-[2..n], n `mod` x == 0]



-- Datatypes
data V3 a = V3 a a a



instance Functor V3 where
    fmap f (V3 a b c) = V3 (f a) (f b) (f c)

instance Foldable V3 where
    foldr f id (V3 a b c) = f a (f b ( f c id))
