# Projektwoche 2022. Funktionale Programmierung.

Dieses Projekt enthält Lösungen und Beispiele für Probleme, die in der Projektwoche thematisiert werden könnten.

## Ablauf
### 09.09. Einführung
 + Setup umgebung 
 + Was ist eine Funktion, Mengen, Abbildungen etc.
 + Einfache Funktionen auf `String`, `Int`, `Double` etc.
 + Typensignaturen einfacher Funktionen
 + Listen
 + map einführen (ohne Details)
 + pattern matching

### 10.09. Typen
 + list comprehensions
 + rekursion (Liste reversen, liste summieren, (array (`[[a]]`) transponieren)
 + Higher order functions: `fmap`, `foldr`, `filter`, etc...
 + Erste kleine Beispiele zeigen und daran Arbeiten
 + Custom Datatypes mit `data`
 + interessante funktionen aus prelude implementieren: (map, foldr, any, filter)
 + weitere funktionen: List join, 

### 10.09. How to solve it/Monaden
 + IO monad, Maybe, Either, `>>=`, `>>`, `return`
 + kleine Beispiele ansehen, ideen sammeln
 + anfangen an den Projekten zu Arbeiten


## Projektideen
 + 2048
 + Basic IUPAC Parser (advanced)
 + Convex Hull
 + basic math engine
 + rpn Rechner
 + color scheme generator (advanced)
 + rotating cube
 + file compressor (advanced)
 + image filters
 + animationen mit reanimate

## Ziel/Motivation
Es wird nicht versucht groessere Anwendungen oder Aehnliches zu schreiben, das Ziel ist vielmehr das Erlernen der Konzepter funktionaler Programmierung. Es wird Beispiele geben, wir werden IO behandeln, aber dies steht nicht im Mittelpunkt der Veranstaltung. Ziel sollte das Verstehen und Anwenden der Konzepte der FP sein.
